/**********************************************
* File: searchengine.cpp
* Author: Aidan Gonzales, Stephen Grisoli, Michael Deranek, Nathaniel Ekwueme
* Email: agonza31@nd.edu, sgrisoli@nd.edu, mderane1@nd.edu, nekwueme@nd.edu
* 
* Simple search engine using AVL Trees. 
**********************************************/
#include "AVLTree.h"
#include <string>
#include <iostream>
using namespace std;

struct URL
{
  string url;
  string webName;

  /********************************************
  * Function Name  : URL
  * Pre-conditions : string u, string w
  * Post-conditions: none
  * 
  * Default URL constructor. 
  ********************************************/
  URL(string u, string w)
    : url{ u }, webName{ w } { }

  /********************************************
  * Function Name  : operator==
  * Pre-conditions : string url
  * Post-conditions: bool
  *  
  * Overloaded == operator. Compares the 'url'
  * members of two URL objects and returns the result.
  ********************************************/
  bool operator==(URL test) const {
    return(this->url == test.url);
  }

  /********************************************
  * Function Name  : operator>
  * Pre-conditions : string url
  * Post-conditions: bool
  *  
  * Overloaded > operator. Compares the 'url'
  * members of two URL objects and returns the result.
  ********************************************/
  bool operator>(URL test) const {
    return(this->url > test.url);
  }

  /********************************************
  * Function Name  : operator<
  * Pre-conditions : string url
  * Post-conditions: bool
  *  
  * Overloaded < operator. Compares the 'url'
  * members of two URL objects and returns the result.
  ********************************************/
  bool operator<(URL test) const {
    return(this->url < test.url);
  }
};

int main()
{
  AVLTree<URL> test; // create tree
  URL u1("http://cse.nd.edu","Notre Dame CSE Homepage");
  URL u2("http://cse.nd.edu/people/faculty","Faculty - Department of Computer Science and Engineering"); 
  URL u3("http://nd.edu","University of Notre Dame");
  URL u4("http://engineering.nd.edu/profiles/mmorrison","Matthew Morrison - College of Engineering"); 
  URL u5("http://engineering.nd.edu/news-publications/engineering-in-the-news","Engineering in the News - College of Engineering");
  URL u6("http://engineering.nd.edu","College of Engineering");
  URL u7("http://cse.nd.edu","Notre Dame CSE Homepage");
  URL u8("http://science.nd.edu/departments","Departments // College of Science // University of Notre Dame");
  URL u9("http://science.nd.edu","College of Science // University of Notre Dame");
  URL u10("http://science.nd.edu/departments/chemistry-biochemistry","Chemistry and Biochemistry // College of Science // University of Notre Dame");

  // inserting elements in tree
  cout << "Pushing u1 to tree" << endl;
  test.insert(u1);
  cout << "u1 successfully pushed to tree." << endl << endl;

  cout << "Pushing u2 to tree" << endl;
  test.insert(u2);
  cout << "u2 successfully pushed to tree." << endl << endl;

  cout << "Pushing u3 to tree" << endl;
  test.insert(u3);
  cout << "u3 successfully pushed to tree." << endl << endl;

  cout << "Pushing u4 to tree" << endl;
  test.insert(u4);
  cout << "u4 successfully pushed to tree." << endl << endl;

  cout << "Pushing u5 to tree" << endl;
  test.insert(u5);
  cout << "u5 successfully pushed to tree." << endl << endl;

  cout << "Pushing u6 to tree" << endl;
  test.insert(u6);
  cout << "u6 successfully pushed to tree." << endl << endl;

  cout << "Pushing u7 to tree" << endl;
  test.insert(u7);
  cout << "u7 successfully pushed to tree." << endl << endl;

  cout << "Pushing u8 to tree" << endl;
  test.insert(u8);
  cout << "u8 successfully pushed to tree." << endl << endl;

  cout << "Pushing u9 to tree" << endl;
  test.insert(u9);
  cout << "u9 successfully pushed to tree." << endl << endl;

  cout << "Pushing u10 to tree" << endl;
  test.insert(u10);
  cout << "u10 successfully pushed to tree." << endl << endl;

  // removing root
  cout << "Removing root (u5)" << endl;
  test.remove(u5);
  cout << "Root successfully removed." << endl << endl;

  // testing overloaded operators
  if(u1 == u7) cout << "u1 == u7" << endl;
  if(u1 > u7) cout << "u1 > u7" << endl;
  if(u1 < u7) cout << "u1 < u7" << endl;

  if(u1 == u2) cout << "u1 == u7" << endl;
  if(u1 < u2) cout << "u1 < u2" << endl;
  if(u1 > u2) cout << "u1 > u2" << endl;

  return 0;
}
