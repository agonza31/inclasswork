/**********************************************
* File: 5b01_BF.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* File for the Brute Force solution to the
* Triple Step problem 
**********************************************/

#include<iostream>

/********************************************
* Function Name  : countways
* Pre-conditions : int n
* Post-conditions: int
*  
* Recursively calls the countways function
********************************************/
int countways(int n){
	if(n < 0)
		return 0;
	else if(n == 0)
		return 1;
	else
		return countways(n-1) + countways(n-2) + countways(n-3);
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char**argv
* Post-conditions: int
* 
* This is the main driver function
* Assumes argv[1] is a valid integer 
********************************************/
int main(int argc, char**argv){

	std::cout << countways(atoi(argv[1])) << std::endl;

	return 0;
}

