/**********************************************
* File: TicTacToeTest.cpp
* Author: Aidan Gonzales
* Email: agonza31@nd.edu
* 
* Implementation to test algorithm that checks
* if a player has won a game of Tic-Tac-Toe.
* Function in question is "hasWon"
*
* It should be noted that, while this is
* done with a conventional 3x3 Tic-Tac-Toe
* board, the algorithm can be easily modified
* for an NxN board by swapping the arguments
* of board (and corresponding instances in
* TTTBoard's methods) with N.
**********************************************/
#include <iostream>
#include <string>

struct TTTBoard {

    char board[3][3];
    char playerTurn = 'X'; // starts at X, by default. Switches to O (or X) when the opposite player moves

    /********************************************
    * Function Name  : TTTBoard
    * Pre-conditions : none
    * Post-conditions: none
    * 
    * Default constructor for TTTBoard struct
    ********************************************/
    TTTBoard() {
        for(int r = 0; r < 3; r++)
          for(int c = 0; c < 3; c++)
            board[c][r] = ' ';
    }
    
    /********************************************
    * Function Name  : fillX
    * Pre-conditions : int r, int c
    * Post-conditions: none
    * 
    * Makes a move for player 1 (places an X in
    * a free spot)
    ********************************************/
    void fillX(int r, int c) {
	if(board[c][r] == ' ') {
          board[c][r] = 'X';
	  hasWon();
	  playerTurn = 'O';
	}
	else std::cout << "Space already filled by " << board[c][r] << ". Choose new space for Player " << playerTurn << std::endl;
    }

    /********************************************
    * Function Name  : fillO
    * Pre-conditions : int r, int c
    * Post-conditions: none
    * 
    * Makes a move for player 2 (places an O in
    * a free spot)
    ********************************************/
    void fillO(int r, int c) {
	if(board[c][r] == ' ') {
          board[c][r] = 'O';
	  hasWon();
	  playerTurn = 'X';
	}
	else std::cout << "Space already filled by " << board[c][r] << ". Choose new space for Player " << playerTurn << std::endl;
    }    

    /********************************************
    * Function Name  : hasWon
    * Pre-conditions : none
    * Post-conditions: void
    * 
    * Determines if a player has won, and if so,
    * states who has won. 
    ********************************************/
    void hasWon() {
        for(int r = 0; r < 3; r++) { // win across a row
	    if((board[0][r] == playerTurn) && (board[1][r] == playerTurn) && (board[2][r] == playerTurn)) { // playerTurn is compared to because only the player who JUST made a move could have a NEW winning combo.
	        std::cout << playerTurn << " has won!" << std::endl;
    	        std::cout << "Winning cells: (" << r << ", 0), (" << r << ", 1), (" << r << ", 2)" << std::endl;   
		return;
	    }
	}

	for(int c = 0; c < 3; c++) { // win down a column
	    if((board[c][0] == playerTurn) && (board[c][1] == playerTurn) && (board[c][2] == playerTurn)) { // see line 104 for explanation of why playerTurn is compared to
	        std::cout << playerTurn << " has won!" << std::endl;
    	        std::cout << "Winning cells: (0, " << c << "), (1, " << c << "), (2, " << c << ")" << std::endl;   
		return;
	    }
	}

	for(int i = 0; i <= 2; i+=2) { // Diagonal wins. This for loop is arranged so that it will test the two diagonal cases--(0,0), (1,1), (2,2) AND (0,2), (1,1), (2,0). Note that for a 2D array, R and C are reversed (ex. the latter diagonal case would be board[2][0], board[1][1], board[0][2])
	  if((board[i][0] == playerTurn) && (board[1][1] == playerTurn) && (board[2 - i][2] == playerTurn)) {
	    std::cout << playerTurn << " has won!" << std::endl;
    	    std::cout << "Winning cells: (0, " << i << "), (1, 1), (2, " << 2 - i << ")" << std::endl;	    
	    return;
	  }
 	}

	std::cout << "No winner." << std::endl;
	return;
    }

    friend std::ostream& operator<<(std::ostream& outStream, const TTTBoard& printBoard);
};

/********************************************
* Function Name  : operator<<
* Pre-conditions : std::ostream& outStream, const TTTBoard& printBoard
* Post-conditions: std::ostream&
* 
* Overloaded outstream operator. Outputs the
* Tic-Tac-Toe board with X's and O's filled
* in as appropriate. Based on code provided
* by Prof. Matthew Morrison (AVLAuthorTest.cpp)
********************************************/
std::ostream& operator<<(std::ostream& outStream, const TTTBoard& printBoard) {
    
    for(int r = 0; r < 3; r++) {
        for(int c = 0; c < 3; c++)
	    outStream << "|" <<  printBoard.board[c][r] << "|";
	outStream << std::endl;
    }

    outStream << std::endl;

    return outStream;
}

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
*
* Main driver for TTTBoard to test hasWon
* function
********************************************/
int main() {

    TTTBoard test;

    std::cout << "Initialized empty board. Note: cells are given by (row, column). top left is (0,0), bottom right is (2,2)" << std::endl;
    std::cout << test;

    // Move 1
    std::cout << "Player " << test.playerTurn << "'s move: (0,1)" << std::endl;
    test.fillX(0,1);
    std::cout << test;

    // Move 2
    std::cout << "Player " << test.playerTurn << "'s move: (0,0)" << std::endl;
    test.fillO(0,0);
    std::cout << test;

    // Move 3
    std::cout << "Player " << test.playerTurn << "'s move: (1,1)" << std::endl;
    test.fillX(1,1);
    std::cout << test;

    // Move 4
    std::cout << "Player " << test.playerTurn << "'s move: (2,1)" << std::endl;
    test.fillO(2,1);
    std::cout << test;

    // Move 5
    std::cout << "Player " << test.playerTurn << "'s move: (1,2)" << std::endl;
    test.fillX(1,2);
    std::cout << test;

    // Move 6
    std::cout << "Player " << test.playerTurn << "'s move: (2,2)" << std::endl;
    test.fillO(2,2);
    std::cout << test;

    // Move 7
    std::cout << "Player " << test.playerTurn << "'s move: (0,0)" << std::endl;
    test.fillX(0,0);
    std::cout << test;

    // Move 8: Testing row win
    std::cout << "Player " << test.playerTurn << "'s move: (1,0)" << std::endl;
    test.fillX(1,0);
    std::cout << test << "-----------------------" << std::endl;
   
    // Testing column win
    TTTBoard test2;
    std::cout << "Initialized NEW empty board. Filled in column win for O" << std::endl;
    test2.fillX(0,0);
    std::cout << test2;
    test2.fillO(0,1);
    std::cout << test2;
    test2.fillX(1,0);
    std::cout << test2;
    test2.fillO(1,1);
    std::cout << test2;
    test2.fillX(2,2);
    std::cout << test2;
    test2.fillO(2,1);
    std::cout << test2 << "----------------------" << std::endl;

    // Testing diagonal wins
    TTTBoard test3;
    std::cout << "Initialized NEW empty board. Filled in diagonal win for X" << std::endl;
    test3.fillX(0,2);
    std::cout << test3;
    test3.fillO(0,1);
    std::cout << test3;
    test3.fillX(1,1);
    std::cout << test3;
    test3.fillO(1,2);
    std::cout << test3;
    test3.fillX(2,0);
    std::cout << test3;

    return 0;
}
