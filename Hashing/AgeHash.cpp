<<<<<<< HEAD
#include <map>
#include <unordered_map>
#include <iterator>
#include <string>
#include <iostream>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argc
* Post-conditions: int
* 
********************************************/
int main(int argc, char** argv){

  std::map<std::string, int> ageHashOrdered = { {"Matthew", 38}, {"Alfred", 70}, {"Roscoe", 2}, {"James", 12} }; // the standard "map" is the ordered one.

  std::map<std::string, int>::iterator iterOr; // for an iterator, used in a for loop. Note the type, "iterator"

  std::cout << "The ordered ages are: " << std::endl; // for the ordered map
  for(iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++){
    std::cout << iterOr->first << " " << iterOr->second << std::endl;
  }

  std::cout << std::endl;

  std::unordered_map<std::string, int> ageHashUnordered = { {"Matthew", 38}, {"Alfred", 70}, {"Roscoe", 2}, {"James", 12} }; // note the use of "unordered_map" instead of "map", as with line 15
  std::unordered_map<std::string, int>::iterator iterUn; // and a separate iterator, to boot.

  std::cout << "The unordered ages are: " << std::endl; // for the unordered map
  for(iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++){
    std::cout << iterUn->first << " " << iterUn->second << std::endl;
  }

  std::cout << std::endl;
  
  std::cout << "The Ordered Example: " << ageHashOrdered["Matthew"] << std::endl; // Note the use of brackets [ ]. Call the string key to find the data.
  std::cout << "The Unordered Example: " << ageHashUnordered["Alfred"] << std:: endl; // same for the unordered.

  // ordered map: good iteration, poor efficiency
  // unordered map: poor iteration, good efficiency, i.e. O(1)

  return 0;

}
=======
/**********************************************
* File: AgeHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************/
// Libraries Here

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(int argc, char** argv){

	
	return 0;
}
>>>>>>> upstream/master
