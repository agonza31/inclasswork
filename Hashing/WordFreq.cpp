#include <unordered_map>
#include <string>
#include <iostream>
#include <fstream>
#include <iterator>

int main(int argc, char** argv) {

	std::unordered_map<std::string, int> wordCounts;

	std::ifstream inFile;
	inFile.open(argv[1]);
	if(!inFile.is_open())
		exit(-1);
		
	std::string word;
	while(inFile >> word) {
		if(wordCounts.count(word) == 0)
			wordCounts.insert( {word, 1} );
		else
			wordCounts[word]++;
	}
	inFile.close();

	std::unordered_map<std::string, int>::iterator iter;
	for(iter = wordCounts.begin(); iter != wordCounts.end(); iter++)
		std::cout << iter->first << " " << iter->second << std::endl;

	return 0;

}
