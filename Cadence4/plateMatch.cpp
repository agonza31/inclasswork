/**********************************************
* File: plateMatch.cpp
* Author: Aidan Gonzales
* Email: agonza31@nd.edu
*  
*
* Google coding problem solution
**********************************************/
#include <iostream>
#include <unordered_map>
#include <string>
#include <iterator>
#include <fstream>
#include <vector>

/********************************************
* Function Name  : readPlate
* Pre-conditions : std::string
* Post-conditions: std::unordered_map<char, int>
* 
* Populates an unordered map for a single license
* plate, keeping track of the number of times
* that each key letter (upper case, A-Z) appears
* in the license plate.
********************************************/
std::unordered_map<char, int> readPlate(std::string);


/********************************************
* Function Name  : findMatch
* Pre-conditions : std::string, std::unordered_map<std::string, std::vector<int>>
* Post-conditions: none
* 
* Finds a shortest matching word in a dictionary
* that contains all of the letters in a given
* license plate. The license plate may have numbers,
* but nly letters are considered in finding the match. 
********************************************/
void findMatch(std::string, std::unordered_map<std::string, std::vector<int>>);

int main(int argc, char** argv) {

	std::unordered_map<char, int> hashPlate;	// map for license plate letters
	std::unordered_map<std::string, std::vector<int>> hashWord;	// map for dictionary words

	// open file
	std::ifstream dictFile;
	dictFile.open(argv[1]);
	if(!dictFile.is_open()){
		std::cout << "File could not be accessed." << std::endl;
		exit(-1);
	}

	std::string word;
	while(dictFile >> word) {
		if(hashWord.count(word) == 0){
			std::vector<int> temp(26);
			for(char letter : word) {
				if(letter < 65 || letter > 90) continue;
				temp[letter - 65]++;
			}
			hashWord.insert( {word, temp} );
		}
		else { // a dictionary should not have any repeat words, so this case does not need to do anything. This assumes a proper dictionary, however.
		}
	}

	dictFile.close();

	// generate license plates
	std::string plate1 = "RCT100SA";
	std::string plate2 = "RT123SO";
	std::string plate3 = "AQ10S0K";
	std::string plate4 = "TNT055RB";
	std::string plate5 = "LET10EE0";
	std::string plate6 = "AEI1O2U3";
	std::string plate7 = "OTR1N2E3";
	std::string plate8 = "AM1E2D3";
	std::string plate9 = "SHR5I3I3";
	std::string plate10 = "RC1001F";
	std::string plate11 = "RC10014";

	// find matches
	findMatch(plate1, hashWord);
	findMatch(plate2, hashWord);
	findMatch(plate3, hashWord);
	findMatch(plate4, hashWord);
	findMatch(plate5, hashWord);
	findMatch(plate6, hashWord);
	findMatch(plate7, hashWord);
	findMatch(plate8, hashWord);
	findMatch(plate9, hashWord);
	findMatch(plate10, hashWord);
	findMatch(plate11, hashWord);

	return 0;
}

std::unordered_map<char, int> readPlate(std::string plate) {
	std::unordered_map<char, int> hashPlate;

	for(char letter : plate) {
		if(letter > 90 || letter < 65) continue;
		if(hashPlate.count(letter) == 0)
			hashPlate.insert( {letter, 1} );
		else
			hashPlate[letter]++; // we MUST account for repeat letters, ex. SHR5I3I3 has two I's
	}

	return hashPlate;
}

void findMatch(std::string p, std::unordered_map<std::string, std::vector<int>> hashW) {

	std::string shortest; // shortest word
	std::unordered_map<char, int> hashP = readPlate(p);
	bool match; // true if the dictionary word compared to is a match; false if not

	std::unordered_map<char, int>::iterator iter;
	std::unordered_map<std::string, std::vector<int>>::iterator wter;

	for(wter = hashW.begin(); wter != hashW.end(); wter++) {
		for(iter = hashP.begin(); iter != hashP.end(); iter++) {
			if((wter->second).at((iter->first) - 65) < (iter->second)) { // If dict word has less of a certain letter than the license plate does, that word CANNOT be the desired match, as it wouldn't contain all of the license plate's letters.
				match = false;
				break;
			}
			match = true;
		}
		if(match && ((wter->first).size() < shortest.size() || shortest.empty())) shortest = wter->first; // we want the shortest word, so we must compare the current match's length to that of the current shortest. If there is no curent shortest, this isn't possible.
	}
	
	std::cout << "Shortest word in dictionary with characters in " << p << ": " << shortest << std::endl;
}
